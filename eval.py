import keras
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid.anchored_artists import AnchoredText
import numpy as np
from keras.utils import to_categorical

dataTest = np.load("test.npz")
X_test, y_test = dataTest["x"], dataTest["y"]
X_test = X_test.reshape((X_test.shape[0], X_test.shape[1], 1))
y_test_cat = to_categorical(y_test)

model = keras.models.load_model("counter.h5")
model.summary()
ev = model.evaluate(X_test, y_test_cat)

fig = plt.figure(figsize=(7, 4))
plt.plot(X_test[0])
ax = fig.gca()
labels = [item.get_text() for item in ax.get_xticklabels()]
empty_string_labels = [''] * len(labels)
ax.set_xticklabels(empty_string_labels)
labels = [item.get_text() for item in ax.get_yticklabels()]
empty_string_labels = [''] * len(labels)
ax.set_yticklabels(empty_string_labels)
plt.savefig("Sample.pdf", bbox_inches="tight")

fig = plt.figure(figsize=(7, 4))
ax = fig.gca()
pred = model.predict(X_test)
pred = np.argmax(pred, axis=1)
diff = pred - y_test
plt.hist(diff)
plt.xlabel(r"$n_p - n$")
at = AnchoredText("$\sigma = %.2f$" % (np.std(diff, ddof=1)),
                  frameon=True, loc=1)
ax.add_artist(at)
plt.savefig("Diff.pdf", bbox_inches="tight")
print()
print("Standard deviation: %.2f" % (np.std(diff, ddof=1)))
print("Test accuracy: %.2f percent" % (ev[1] * 100))
plt.show()
