import numpy as np
import matplotlib.pyplot as plt


def gauss(x, *args):
    return args[0] * np.exp(-(x - args[2])**2 / (2 * args[1]**2))


def lorentz(x, *args):
    return args[0] / (1 + (x - args[2])**2 / args[1]**2)


A, sigma = np.linspace(0, 1, 1000), np.linspace(0.00001, 5, 1000)
x0 = np.linspace(-100, 100, 1000)
x = np.linspace(-100, 100, 1000)


def nextPeak(x):
    Ai, sigmai = np.random.choice(A), np.random.choice(sigma)
    x0i = np.random.choice(x0)
    func = np.random.choice([gauss, lorentz])
    return func(x, Ai, sigmai, x0i)


def gen():
    nPeaks = np.random.randint(1, 10)
    out = np.zeros(len(x))
    for i in range(nPeaks):
        out += nextPeak(x)
    return nPeaks, out


for n, fname in zip([10000, 1000], ["train", "test"]):
    data = np.zeros((n, len(x)))
    numberOfPeaks = np.zeros(n)
    for i in range(n):
        nPeaks, step = gen()
        data[i, :] = step
        numberOfPeaks[i] = nPeaks
    np.savez_compressed(fname + ".npz", x=data, y=numberOfPeaks)
