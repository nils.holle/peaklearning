import sys

import keras
from keras.layers import *
from keras.optimizers import Adam
from keras.utils import to_categorical
import numpy as np
import matplotlib.pyplot as plt

if len(sys.argv) > 1:
    epochs = int(sys.argv[1])
else:
    epochs = 10

dataTrain = np.load("train.npz")
X_train, y_train = dataTrain["x"], dataTrain["y"]
X_train = X_train.reshape((X_train.shape[0], X_train.shape[1], 1))
y_train = to_categorical(y_train)

inp = Input(shape=(1000, 1))
z = BatchNormalization()(inp)
z = Conv1D(32, 11, activation="relu")(z)
z = Conv1D(32, 9, activation="relu")(z)
z = MaxPooling1D(3)(z)
z = Conv1D(64, 7, activation="relu")(z)
z = Conv1D(64, 5, activation="relu")(z)
z = MaxPooling1D(3)(z)
z = Conv1D(128, 3, activation="relu")(z)
z = Conv1D(128, 3, activation="relu")(z)
z = GlobalMaxPooling1D()(z)
z = Dropout(0.25)(z)
z = Dense(512, activation="relu")(z)
out = Dense(10, activation="softmax")(z)

model = keras.models.Model(inputs=inp, outputs=out)
model.compile(optimizer=Adam(lr=0.01, decay=0.12),
              loss="categorical_crossentropy",
              metrics=["acc"])

print(model.summary())
fit = model.fit(x=X_train, y=y_train, epochs=epochs, validation_split=0.1,
                batch_size=512)
model.save("counter.h5")

plt.figure(figsize=(7, 4))
plt.plot(fit.history["loss"], label="loss")
plt.plot(fit.history["val_loss"], label="validation loss")
plt.xlabel("epoch")
plt.legend()
plt.savefig("Loss.pdf", bbox_inches="tight")

plt.figure(figsize=(7, 4))
plt.plot(fit.history["acc"], label="accuracy")
plt.plot(fit.history["val_acc"], label="validation accuracy")
plt.xlabel("epoch")
plt.legend()
plt.savefig("Accuracy.pdf", bbox_inches="tight")
